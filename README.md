### Prerequisites

Before you begin, ensure you have met the following requirements:

- Node.js installed. This application requires Node.js **16.20.2**
- npm (Node Package Manager) installed. This application is managed using npm **8.19.4**
- Angular CLI installed globally. You can install it using the following command: "**npm install -g @angular/cli**"

### Getting Started

To run the application, follow these steps:
1. Clone the repository
2. Navigate to the project directory
3. install dependencies using "**npm install**"
4. Run the project using "**ng serve**"
5. Open a web browser and go to http://localhost:4200 to view the application.

### Usage

The 5 questions will only appear if the backend ([Trivia-backend](https://gitlab.com/kylevr1994/trivia-backend)) is running.
