import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface QuizQuestion {
  id: number;
  category: string;
  type: string;
  difficulty: string;
  question: string;
  answers: string[];
  selectedAnswer: string | null;
  isCorrect?: boolean; 
}

interface ServerResponse {
  id: number;
  answer: string;
  isCorrect: boolean;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'trivia-frontend';

  public quizData: QuizQuestion[] = [
    
  ];


  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.http.get<any[]>('http://localhost:8080/questions')
      .subscribe(data => {
        this.quizData = data;

      });
  }

  sendAnswer() {
    const submissionData = this.quizData
      .filter(question => question.selectedAnswer !== null)
      .map(({ id, selectedAnswer }) => ({ id, answer: selectedAnswer }));
  
      this.http.post<ServerResponse[]>('http://localhost:8080/checkanswers', submissionData)
      .subscribe(responses => {
        responses.forEach((response, index) => {
          this.quizData[index].isCorrect = response.isCorrect;
        });
      });
  }
  

}
